#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from datetime import datetime
import base64
import calendar
import logging
import os
import sqlite3
import sys
import typing

from InvoiceGenerator.api import Client, Creator, Invoice, Item, Provider
from InvoiceGenerator.pdf import SimpleInvoice, prepare_invoice_draw
from reportlab.lib.units import mm
import python_http_client.exceptions
import sendgrid
import sendgrid.helpers.mail as mail
import yaml

os.environ["INVOICE_LANG"] = "en"


class MyPdfInvoice(SimpleInvoice):
    """Overriding SimpleInvoice to change graphics for additional fields."""

    def gen(self, filename, generate_qr_code=False):
        self.filename = filename
        self.qr_builder = None

        prepare_invoice_draw(self)

        self._drawMain()
        self._drawTitle()
        self._drawProvider(self.TOP - 10, self.LEFT + 3)
        self._drawClient(self.TOP - 41, self.LEFT + 91)
        self._drawPayment(self.TOP - 37, self.LEFT + 3)
        self._drawQR(self.TOP - 39.4, self.LEFT + 61, 75.0)
        self._drawDates(self.TOP - 10, self.LEFT + 91)
        self._drawItems(self.TOP - 80, self.LEFT)

        self.pdf.showPage()
        self.pdf.save()

    def _drawMain(self):
        self.pdf.rect(
            self.LEFT * mm,
            (self.TOP - 68) * mm,
            (self.LEFT + 156) * mm,
            65 * mm,
            stroke=True,
            fill=False,
        )

        path = self.pdf.beginPath()
        path.moveTo((self.LEFT + 88) * mm, (self.TOP - 3) * mm)
        path.lineTo((self.LEFT + 88) * mm, (self.TOP - 68) * mm)
        self.pdf.drawPath(path, True, True)

        path = self.pdf.beginPath()
        path.moveTo(self.LEFT * mm, (self.TOP - 29) * mm)
        path.lineTo((self.LEFT + 88) * mm, (self.TOP - 29) * mm)
        self.pdf.drawPath(path, True, True)

        path = self.pdf.beginPath()
        path.moveTo((self.LEFT + 88) * mm, (self.TOP - 29) * mm)
        path.lineTo((self.LEFT + 176) * mm, (self.TOP - 29) * mm)
        self.pdf.drawPath(path, True, True)


def setup_logging() -> logging.Logger:
    file_handler = logging.FileHandler("invoicing.log")
    file_handler.setLevel(logging.DEBUG)

    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setLevel(logging.INFO)

    # not strictly necessary, but can be useful if we want to
    # shell redirect ERROR and CRITICAL logs to a file or something
    error_handler = logging.StreamHandler()  # defaults to stderr
    error_handler.setLevel(logging.ERROR)

    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    for handler in (file_handler, console_handler, error_handler):
        handler.setFormatter(formatter)

    logging.basicConfig(
        level=logging.INFO,
        handlers=[file_handler, console_handler, error_handler],
    )

    return logging.getLogger("invoicing")


def load_invoice_date(config: dict[str, typing.Any]) -> datetime:
    """Return the invoice date.

    Return the current date if invoice_date not provided
    or if the provided value is invalid.
    """
    try:
        date = config["invoice"]["date"]
        return datetime.fromisoformat(date)
    except Exception:
        return datetime.today()


def load_due_date(config: dict[str, typing.Any]) -> datetime:
    """Return the invoice due date.

    Return the last day of the current month if not provided
    or if the provided value is invalid.
    """
    try:
        date = config["invoice"]["due_date"]
        return datetime.fromisoformat(date)
    except Exception:
        return get_month_end()


def get_month_end() -> datetime:
    """Return last day of the month as datetime object."""
    today = datetime.today()
    month_range = calendar.monthrange(today.year, today.month)
    return datetime.fromisoformat(
        f"{today.year}-{today.strftime('%m')}-{month_range[1]}"
    )


def db_connect(database: str = "invoices.db") -> sqlite3.Connection:
    """Return a connection to the invoices database."""
    return sqlite3.connect(database=database, detect_types=sqlite3.PARSE_DECLTYPES)


def invoice_table_exists(db: sqlite3.Connection, table: str = "invoices") -> bool:
    """Return `True` if the invoices table exists, `False` otherwise."""
    with db:
        res = db.execute(f"SELECT name FROM sqlite_master WHERE name='{table}'")

    if res.fetchone() is None:
        return False

    return True


def invoice_table_create(db: sqlite3.Connection):
    """Create invoices table."""
    with db:
        db.execute("CREATE TABLE invoices(number INTEGER, date, client, total REAL)")


def invoice_get_next_number(db: sqlite3.Connection) -> int:
    """Return the next invoice number to use for the current invoice."""
    with db:
        res = db.execute(
            "SELECT * FROM invoices WHERE ROWID in ( SELECT max( ROWID ) FROM invoices );"
        )
        last_inv = res.fetchone()

    if last_inv is None:
        return 1

    return int(last_inv[0]) + 1


def mail_prepare(config: dict[str, typing.Any]) -> mail.Mail:
    """Return an initially prepared email message."""
    provider_name = config["provider"]["summary"]
    c = config["sendgrid"]

    from_email = mail.From(c["from"], provider_name)
    to_email = mail.To(c["to"], c["salutation"])
    cc = c.get("cc")

    message = mail.Mail(
        from_email=from_email,
    )

    personalization = mail.Personalization()
    personalization.add_email(to_email)

    if cc:
        cc_email = mail.Cc(cc, c.get("cc_salutation", ""))
        personalization.add_email(cc_email)

    message.add_personalization(personalization=personalization)

    return message


def mail_finalize(
    invoice: Invoice, mailcfg: dict, filename: str, message: mail.Mail
) -> mail.Mail:
    """Return an email message that is ready to send."""
    message.subject = f"Invoice {invoice.number} from {invoice.provider.summary}"

    content = mail.PlainTextContent(
        f"""
Hi {mailcfg["salutation"]},

A new invoice has been generated for you by {invoice.provider.summary}. Here's a quick summary:

Invoice: {invoice.number}

Total Amount Due: {invoice.currency}{invoice.price_tax}

Due Date: {invoice.payback.strftime("%d. %b %Y")}

You can find the generated invoice attached to this email.

Best regards,
{invoice.provider.summary}
    """
    )

    message.add_content(
        content=content,
        mime_type=mail.MimeType.text,
    )

    with open(filename, "rb") as f:
        data = f.read()
        f.close()

    encoded_file = base64.b64encode(data).decode()
    attachment = mail.Attachment(
        mail.FileContent(encoded_file),
        mail.FileName(filename),
        mail.FileType("application/pdf"),
        mail.Disposition("attachment"),
    )
    message.attachment = attachment

    return message


if __name__ == "__main__":
    log = setup_logging()
    log.info("Invoicing started...")

    c = yaml.safe_load(open("config.yaml"))
    log.info("YAML configuration loaded.")

    sg = None
    prep_email = None
    if c.get("sendgrid", {}).get("api_key", ""):
        log.info("Mail: sendgrid delivery enabled.")
        sg = sendgrid.SendGridAPIClient(api_key=c["sendgrid"]["api_key"])
        prep_email = mail_prepare(config=c)
        log.info("Mail: message initialized for %s", prep_email.from_email.email)
    else:
        log.info("Mail: delivery disabled, no API key configured.")

    # do database setup if a DB name is configured
    db = None
    db_name = c.get("db", {}).get("name", "")
    if db_name:
        log.info("DB: support enabled for %s", db_name)
        db = db_connect(db_name)

        if not invoice_table_exists(db):
            log.info("DB: creating invoices table...")
            invoice_table_create(db)

        invoice_number = invoice_get_next_number(db)
    else:
        log.info("DB: support not enabled, no config found.")
        # if we don't get invoice from the DB then try the config
        # otherwise, fall back to invoice #1
        invoice_number = c["invoice"].get("number", 1)

    log.info("Preparing invoice: %s", invoice_number)

    invoice_date = load_invoice_date(c)
    due_date = load_due_date(c)
    expense_report = c.get("invoice", {}).get("expensify_report", "")
    expense_total = c.get("invoice", {}).get("expense_total", "")

    creator = Creator(
        name=c["creator"]["name"],
        stamp_filename=c["creator"].get("stamp_filename", ""),
    )

    client = Client(
        summary=c["client"]["summary"],
        address=c["client"]["address"],
        address2=c["client"].get("address2", ""),
        city=c["client"]["city"],
        zip_code=c["client"]["zip_code"],
        country=c["client"]["country"],
        vat_id=c["client"].get("vat_id", ""),
    )

    provider = Provider(
        summary=c["provider"]["summary"],
        address=c["provider"]["address"],
        address2=c["provider"].get("address2", ""),
        city=c["provider"]["city"],
        zip_code=c["provider"]["zip_code"],
        country=c["provider"]["country"],
        email=c["provider"].get("email", ""),
        bank_name=c["provider"].get("bank_name", ""),
        bank_address=c["provider"].get("bank_address", ""),
        bank_address2=c["provider"].get("bank_address2", ""),
        bank_city=c["provider"].get("bank_city", ""),
        bank_zip_code=c["provider"].get("bank_zip_code", ""),
        bank_country=c["provider"].get("bank_country", ""),
        bank_account=c["provider"].get("bank_account", ""),
        bank_routing=c["provider"].get("bank_routing", ""),
    )

    invoice = Invoice(client=client, provider=provider, creator=creator)
    log.info("Client for this invoice: %s", invoice.client.summary)

    consulting_desc = f"Consulting services for {invoice_date.strftime('%B %Y')}"
    expensify_desc = f"Expensify report #{expense_report}"

    invoice.currency_locale = os.environ["LANG"]
    invoice.currency = "$"
    invoice.swift = c["provider"]["swift"]
    invoice.number = f"DS-{invoice_number}"
    invoice.date = invoice_date
    invoice.payback = due_date
    invoice.title = "Invoice"
    invoice.add_item(
        Item(count=1, price=c["provider"]["salary"], description=consulting_desc)
    )

    if expense_report:
        log.info("Expense report added: #%s", expense_report)
        invoice.add_item(
            Item(count=1, price=f"{expense_total}", description=expensify_desc)
        )

    pdf = MyPdfInvoice(invoice=invoice)
    pdf_filename = f"{invoice.number}.pdf" if invoice.number else f"invoice.pdf"
    log.info("Writing invoice to %s...", pdf_filename)
    pdf.gen(pdf_filename)

    if prep_email is not None:
        log.info("Mail: finalizing message...")
        final_msg = mail_finalize(
            invoice=invoice,
            mailcfg=c["sendgrid"],
            filename=pdf_filename,
            message=prep_email,
        )

        try:
            log.info("Mail: sending mail over the wire...")
            response = sg.client.mail.send.post(request_body=final_msg.get())
        except python_http_client.exceptions.HTTPError as e:
            log.error(
                "Sendgrid HTTPError: %s - Reason: %s\n%s",
                e.status_code,
                e.reason,
                e.to_dict,
            )
            raise
        except Exception as e:
            log.exception("Sendgrid Exception: %s", e)
            raise

        log.info("Mail: message sent successfully.")

    # if we're using a database, insert the invoice data
    if db is not None:
        data = (
            int(invoice_number),
            str(invoice_date),
            str(invoice.client.summary),
            float(invoice.price_tax),
        )
        log.info("DB: inserting invoice data into invoices table...")
        with db:
            db.execute("INSERT INTO invoices VALUES (?, ?, ?, ?)", data)

        log.info("DB: invoice data inserted successfully.")
